package com.playtomic.tests.wallet.repository;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.playtomic.tests.wallet.entity.WalletEntity;

@Repository
public interface WalletRepository extends JpaRepository<WalletEntity, BigDecimal> {
	
	Optional<WalletEntity> findById(BigDecimal id);

}
