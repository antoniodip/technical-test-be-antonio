package com.playtomic.tests.wallet.service;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.playtomic.tests.wallet.dto.WalletDTO;
import com.playtomic.tests.wallet.entity.WalletEntity;
import com.playtomic.tests.wallet.repository.WalletRepository;

@Service
public class WalletService {
	
	private Logger log = LoggerFactory.getLogger(WalletService.class);
	
	@Autowired
	private WalletRepository repo;
	
	@Autowired
	private StripeService stripeService;
	
	public WalletDTO findById(final BigDecimal id) {
		log.debug("Retrieving wallet by id: [{}]", id);
		WalletEntity wallet = repo.findById(id).orElseThrow(() -> new RuntimeException("Wallet not found for id: " + id));

		log.debug("Wallet found: [{}]", wallet);
		
		/*** NOTE: normally I would use a mapping library as mapstruct to convert entity to dto
		 * but here I just use lombok for simplicity
		 ***/
		return WalletDTO.builder()
				.id(wallet.getId())
				.balance(wallet.getBalance())
				.build();
	}
	
	@Transactional
	public WalletDTO topUpMoneyInWallet(final BigDecimal walletId, final BigDecimal amount, final String creditCardNumber)
		throws StripeServiceException, StripeAmountTooSmallException {
		
		log.debug("Top-up money in wallet: [{}]", walletId);		

		// call stripe service to manage payment
		stripeService.charge(creditCardNumber, amount);
		
		/* can't make it work, I keep having the following exception when calling charge service: 
		 * java.lang.IllegalArgumentException: URI is not absolute
		 */ 
		
		// if payment is ok, then update the wallet
		WalletEntity wallet = repo.findById(walletId).orElseThrow(() -> new RuntimeException("Wallet not found for id: " + walletId));
		wallet.increaseBalance(Double.valueOf(amount.toString()));
		
		wallet = repo.save(wallet);
		
		return WalletDTO.builder()
				.id(wallet.getId())
				.balance(wallet.getBalance())
				.build();
	}

}
