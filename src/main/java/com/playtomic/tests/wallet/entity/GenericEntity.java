package com.playtomic.tests.wallet.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@Entity
@Table(name = "GENERIC_ENTITY")
@SuperBuilder
public class GenericEntity implements Serializable {

	private static final long serialVersionUID = 8939244500809489431L;
	
	@Column(name = "DTYPE", insertable = false, updatable = false)
	private String dtype;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private BigDecimal id;
	
	@Column(name = "BALANCE")
	private Double balance;

}
