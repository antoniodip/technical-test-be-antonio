package com.playtomic.tests.wallet.entity;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@SuperBuilder
public class WalletEntity extends GenericEntity {

	private static final long serialVersionUID = -724763510943000055L;

	public void increaseBalance(final Double amount) {
		this.setBalance(this.getBalance() + amount);
	}

}
