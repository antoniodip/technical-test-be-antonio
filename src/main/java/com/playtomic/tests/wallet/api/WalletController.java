package com.playtomic.tests.wallet.api;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.playtomic.tests.wallet.dto.WalletDTO;
import com.playtomic.tests.wallet.service.WalletService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api/v1/wallet", produces = {MediaType.APPLICATION_JSON_VALUE})
@Api
public class WalletController {
	
    private Logger log = LoggerFactory.getLogger(WalletController.class);
    
    @Autowired
    private WalletService service;

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Retrieve a wallet by ID", response = WalletDTO.class)
    public ResponseEntity<WalletDTO> getById(@PathVariable(name = "id", required = true) BigDecimal id) {
        log.info("Logging from [{}]", this.getClass());        
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }
    
    @PutMapping(value = "/{id}")
    @ApiOperation(value = "Top-up money in a wallet", response = WalletDTO.class)
    public ResponseEntity<WalletDTO> updateWallet(@PathVariable(name = "id", required = true) BigDecimal id,
    		@RequestParam(name = "amount", required = true) BigDecimal amount,
    		@RequestParam(name = "creditCardNumber", required = true) String creditCardNumber) {
        log.info("Logging from [{}]", this.getClass());        
        return new ResponseEntity<>(service.topUpMoneyInWallet(id, amount, creditCardNumber), HttpStatus.OK);
    }
    
    /***
     * NOTES:
     * I am assuming that all the data are exchanged in plain text (not encrypted). Ideally sensitive data
     * (i.e. wallet id, credit card number) should be exchanged encrypted and then decrypted when received at BE side.
     ***/
}
