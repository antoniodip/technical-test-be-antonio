package com.playtomic.tests.wallet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Data
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public class WalletDTO extends GenericDTO {

	private static final long serialVersionUID = 2228363353548596643L;
	
	@NonNull
	@JsonProperty("the wallet balance")
	private Double balance;

}
