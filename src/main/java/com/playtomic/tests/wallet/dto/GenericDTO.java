package com.playtomic.tests.wallet.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
public class GenericDTO implements Serializable {
	
	private static final long serialVersionUID = 4670419030897567103L;
	
	@NonNull
	@JsonProperty("the wallet id")
	private BigDecimal id;

}
