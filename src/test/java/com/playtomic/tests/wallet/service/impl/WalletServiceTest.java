package com.playtomic.tests.wallet.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.playtomic.tests.wallet.dto.WalletDTO;
import com.playtomic.tests.wallet.entity.WalletEntity;
import com.playtomic.tests.wallet.repository.WalletRepository;
import com.playtomic.tests.wallet.service.WalletService;

/**
 * 
 * @author antonio.di.pietro
 * {@link WalletService}
 *
 */
@SpringBootTest
public class WalletServiceTest {
	
	@InjectMocks
	private WalletService service;
	@Mock
	private WalletRepository repo;
	
	@Test
	public void testIsFindByIdOK() {
		BigDecimal id = BigDecimal.ONE;
		
		when(repo.findById(Mockito.any(BigDecimal.class))).thenReturn(Optional.of(WalletEntity.builder()
				.id(id).balance(Double.valueOf("10")).build()));
		
		WalletDTO response = service.findById(id);
		
		assertNotNull(response);
		assertEquals(id, response.getId());
	}

}
